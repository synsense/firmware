# Firmware

This repo contains FX3 and FPGA firmware for various SynSense development kits. 

Please go to `Deploy` -> `Package Registry` for firmware files. 